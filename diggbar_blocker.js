Drupal.behaviors.diggBarBlocker = function() {
  if (top !== self && document.referrer.match(/digg\.com\/\w{1,8}/)) {
    if (Drupal.settings.diggbar_blocker_redirect) {
      top.location.replace(Drupal.settings.diggbar_blocker_redirect);
    }
    else {
      top.location.replace(self.location.href);
    }
  }
};


